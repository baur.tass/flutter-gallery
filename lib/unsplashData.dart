class UnsplashData {
  String id;
  String urlSmall;
  String urlFull;
  String description;
  String altDescription;
  String userId;
  String userName;
  String userFirstName;
  String userLastName;

  UnsplashData({
    this.id,
    this.urlSmall,
    this.urlFull,
    this.description,
    this.altDescription,
    this.userId,
    this.userName,
    this.userFirstName,
    this.userLastName
  });
}
