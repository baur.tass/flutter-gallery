import 'package:flutter/material.dart';
import 'package:gallery_app/photoViewer.dart';
import 'package:gallery_app/unsplashData.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class GalleryList extends StatefulWidget{
  @override
  _GalleryListState createState() => _GalleryListState();
}

  class _GalleryListState extends State<GalleryList> {
    Future<List<UnsplashData>> _getData() async {
      final response = await http.get("https://api.unsplash.com/photos/?client_id=896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043");
      var jsonResponse = json.decode(response.body);
      List<UnsplashData> data = [];

      for(var u in jsonResponse){
          UnsplashData photo = UnsplashData(
            id: u['id'], 
            urlSmall: u['urls']['small'], 
            urlFull: u['urls']['full'], 
            description: u['description'], 
            altDescription: u['alt_description'], 
            userFirstName: u['user']['first_name'], 
            userId: u['user']['id'], 
            userLastName: u['user']['last_name'], 
            userName: u['user']['name']
          );
          data.add(photo);
      }
      return data;
    } 


    @override
    Widget build(BuildContext context){
      return Scaffold(
        appBar: AppBar(
          title : Text("All Photo")
        ),
        body: Container(
          child: FutureBuilder(
            future:_getData(),
            builder: (BuildContext context, AsyncSnapshot snapshot){
              if(snapshot.data == null){
                return Container(
                  child: Center(
                    child: Text("Loading"),
                  ),  
                );
              }
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index){
                  return ListTile(
                    leading: Image(
                      image: NetworkImage(snapshot.data[index].urlSmall),
                      width: 50,
                      height:100,
                    ),
                    title: Text(snapshot.data[index].userName),
                    subtitle: Text(
                      snapshot.data[index].description !=null ? snapshot.data[index].description : snapshot.data[index].altDescription,
                      style: TextStyle(fontStyle: FontStyle.italic),
                      ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => PhotoViewer(url: snapshot.data[index].urlFull),
                      ),);
                    },
                  );
                }
              );
            },
          ),
        ),
      );
    }
  }
    
  
